﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Petbook.Models;
using Petbook.Tests.Controllers;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Petbook.Tests
{
    class TestPetDbSet : TestDbSet<Pet>
    {
        [TestMethod]
        public override Pet Find(params object[] keyValues)
        {
            return this.SingleOrDefault(pet => pet.Id == (int)keyValues.Single());
        }
    }
}
