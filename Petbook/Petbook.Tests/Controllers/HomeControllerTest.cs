﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Petbook;
using Petbook.Controllers;
using Petbook.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace Petbook.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void Pets()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Pets() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void AddNewPet()
        {
            // Arrange

            // Act

            // Assert

        }
        [TestMethod]
        public void DeletePet()
        {
            Pet p = new Pet();
            //DeletePet(p);
            Assert.IsNotNull(p);
        }

        [TestMethod]
        public void GetPets()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Pets() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void UpdatePet()
        {
            // Arrange

            // Act

            // Assert
        }
    }
}
