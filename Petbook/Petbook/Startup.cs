﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Petbook.Startup))]
namespace Petbook
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
