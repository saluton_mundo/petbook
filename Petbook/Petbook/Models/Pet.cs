﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Petbook.Models
{
    public class Pet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool isChipped { get; set; }
        public string Description { get; set; }
    }
}