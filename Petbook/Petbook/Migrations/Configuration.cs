namespace Petbook.Migrations
{
    using Petbook.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Petbook.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Petbook.Models.ApplicationDbContext context)
        {
            context.Pets.AddOrUpdate(
               p => p.Id,
               new Pet { Name = "Nemo", Type = "Fish", isChipped = false, Description = "His Dad is really worried. Afraid of Sharks called Bruce. Please check 43 Wallaby Way, Sydney." },
                   new Pet { Name = "Gremlin", Type = "Cat", isChipped = false, Description = "Last seen in the Colchester area. Has distinct ginger fur and a brown collar." },
                   new Pet { Name = "Puds", Type = "Cat", isChipped = false, Description = "Puds is a tabby and does not have a collar." },
                   new Pet { Name = "Cassie", Type = "Dog", isChipped = true, Description = "Cassie is a Springer Spaniel with a purple collar and a distinct think tail." },
                   new Pet { Name = "Robbie", Type = "Cat", isChipped = true, Description = "Black as the night with golden eyes. You can't miss him (unless it's night)" }
           );
        }
    }
}
