﻿var ViewModel = function () {

    // Variables and Objects Set-up
    var PetsUri = '/api/Pets/';
    var self = this;
    self.pets = ko.observableArray();
    self.error = ko.observable();
    self.detail = ko.observable();
    self.edit = ko.observable();
    self.filterText = ko.observable("");
    self.count = ko.observableArray();

    // New Pet Data 
    self.newPet = {
        Name: ko.observable(),
        Type: ko.observable(),
        isChipped: ko.observable(),
        Description: ko.observable()
    }

    // Edit Pet Data
    self.samePet = {
        Id: ko.observable(),
        Name: ko.observable(),
        Type: ko.observable(),
        isChipped: ko.observable(),
        Description: ko.observable()
    }

    // HTTP GET Pet Method
    self.getPetDetail = function (item) {
        ajaxHelper(PetsUri + item.Id, 'GET').done(function (data) {
            self.detail(data);
        });
    }

    // HTTP GET Update Pet Details Method
    self.editPet = function (item) {
        ajaxHelper(PetsUri + item.Id, 'GET').done(function (data) {
            self.edit(data);
        });
    }

    // HTTP PUT Update Pet Method
    self.updatePet = function (formElement) {
        var pet = {
            Id: self.samePet.Id(),
            Name: self.samePet.Name(),
            Type: self.samePet.Type(),
            isChipped: self.samePet.isChipped(),
            Description: self.samePet.Description()
        };

        ajaxHelper(PetsUri+pet.Id, 'PUT', pet).done(function (item) {
            self.pets.push(item);
        });
    }

    // HTTP DELETE Pet Method
    self.deleterec = function (item) {
        $.ajax({
            type: "DELETE",
            url: PetsUri + item.Id,
            success: function (data) {
                getAllPets();//Refresh the Table
            },
            error: function (error) {
                alert(error.status + " " + error.statusText);
            }
        });
    }

    // HTTP POST New Pet Method
    self.addPet = function (formElement) {
        var pet = {
            Name: self.newPet.Name(),
            Type: self.newPet.Type(),
            isChipped: self.newPet.isChipped(),
            Description: self.newPet.Description()
        };

        ajaxHelper(PetsUri, 'POST', pet).done(function (item) {
            self.pets.push(item);
        });
    }

    // HTTP GET Display Pets Method
    function getAllPets() {
        ajaxHelper(PetsUri, 'GET').done(function (data) {
            self.pets(data);
        });
    }

    // AJAX Helper Method
    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    // Search Pet Method
    self.filteredPets = ko.computed(function () {
        if (self.filterText().length > 0) {
            var petsArray = self.pets();
            return ko.utils.arrayFilter(petsArray, function (pet) {
                return ko.utils.stringStartsWith(pet.Name.toLowerCase(), self.filterText())
            });
        }
    });

    // Count Lost Pets Method
    self.count = ko.computed(function () {
        var reports = self.pets();
        return reports.length;
    });

    // Textarea Characters Method
    $('#inputDescription').keyup(function () {
        if (this.value.length > 250) {
            return false;
        }
        $("#remaining").html("Remaining characters: " + (250 - this.value.length));

    });

    // Fetch the initial data.
    getAllPets();
};

ko.applyBindings(new ViewModel());